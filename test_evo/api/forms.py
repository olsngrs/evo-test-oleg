from django import forms

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
import datetime

from .models import TempFile


class TempFileForm(forms.ModelForm):

    class Meta:
        model = TempFile

        fields = ['file', 'life_time']

