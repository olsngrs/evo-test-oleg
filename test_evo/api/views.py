from django.http import JsonResponse, Http404
from django.views.generic import View
from django.db.models import F
from django.shortcuts import get_object_or_404
from django.utils import timezone

from .models import TempFile
from .forms import TempFileForm
from .tasks import delete_temp_file

import datetime

# Create your views here.


class TempFileView(View):

    def get(self, request):
        pk = request.GET.get('file_id', -1)

        temp_file = get_object_or_404(TempFile, pk=pk)

        is_file_alive = timezone.now() - temp_file.upload_time < temp_file.life_time
        if is_file_alive:
            temp_file_url = temp_file.file.url
            return JsonResponse({'file': temp_file_url}, status=200)
        raise Http404

    def post(self, request):
        form = TempFileForm(request.POST, request.FILES)

        if form.is_valid():
            temp_file = form.save()

            delete_temp_file.apply_async((temp_file.id, ), countdown=temp_file.life_time.seconds)

            response_data = {'file_id': temp_file.id}

            return JsonResponse(response_data)
        raise Http404


class ListTempFileView(View):

    def get(self, request):

        queryset = TempFile.objects.annotate(time_left=timezone.now()-F('upload_time'))
        # queryset = queryset.filter(life_time__gte=F('time_left'))

        response_queryset = list(queryset.values())
        return JsonResponse({'data': response_queryset})


