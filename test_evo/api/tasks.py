from django.shortcuts import get_object_or_404, Http404

from test_evo.celery import app

from .models import TempFile


@app.task
def delete_temp_file(file_id):
    temp_file = get_object_or_404(TempFile, pk=file_id)
    temp_file.file.delete()
    temp_file.delete()
